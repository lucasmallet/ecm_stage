source ~/miniconda3/etc/profile.d/conda.sh # solution pour contourner le conda init qui demande de relancer le shell
conda activate cutadaptenv

cd /home/adrien/Desktop/analyse_louise/site_resultats_louise/data/raw_seq
mkdir -p wo_primers

for i in *_R1_001.fastq.gz
do
  SAMPLE=$(echo ${i} | sed "s/_R1_\001\.fastq\.gz//")
  echo ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz
cutadapt --cores=4 --discard-untrimmed -g "GCATCGATGAAGAACGCAGC" -G "TCCTCCGCTTATTGATATGC" -o wo_primers/${SAMPLE}_R1_001.fastq.gz -p wo_primers/${SAMPLE}_R2_001.fastq.gz ${SAMPLE}_R1_001.fastq.gz ${SAMPLE}_R2_001.fastq.gz
done


