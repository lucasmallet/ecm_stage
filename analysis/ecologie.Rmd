---
title: "Ecological analysis"
site: workflowr::wflow_site
output:
  workflowr::wflow_html:
    code_folding: hide
    css: style.css
    code_download: true
editor_options:
  chunk_output_type: console
---

```{r knitr set up, include=FALSE}
library(knitr)
hooks <- knitr::knit_hooks$get()
hook_foldable <- function(type, background_color) {
  force(type)
  function(x, options) {
    res <- hooks[[type]](x, options)

    if (isFALSE(options[[paste0("fold.", type)]])) {
      return(res)
    }

    paste0(
      "<details><summary style ='background-color: ",
      background_color,
      "'>",
      type,
      "</summary>\n\n",
      res,
      "\n\n</details>"
    )
  }
}
knitr::knit_hooks$set(
  output = hook_foldable("output", background_color = "#a6d3e3"),
  message = hook_foldable("message", background_color = "#fae2df "),
  warning = hook_foldable("warning", background_color = "#dc863b")
)

opts_chunk$set(
  fig.align = "center",
  fig.retina = 2,
  fig.width = 10
)
options(scipen = 3, digits = 3)
```

# QUESTIONS

Each question need a particular focus on ECM species but all analysis will be also interesting regarding other guilds.

## Methodological aspect

1. Positive control 
2. Effect of sequencing depth 
3. Comparaison : Sanger + bain (run 5) + mycorrhize associé (run4)


THEN : Merge positive controls and samples with different sequencing depth for subsequent analysis.


## Operational aspect

Quelles sont les moyens de réensemencer les sols urbains en champignons? Avec quelles doses (dilution), quels sols (type de sol) et quels délais (Pt_temporel)?

1. Dilution
2. Pt_temporel 
3. Soil type (Cistus, Quercus, Pinus)

## Fundamental aspect

Urban soil resilience  :
Hypothesis : more Asco and hypogeous species


# Analysis

## Loading function and packages

```{r Source packages, message=FALSE, results='hide'}
source("code/packages.R")
```

```{r Source function, message=FALSE, results='hide'}
source("code/functions.R")
file.sources <- list.files(
  "~/Nextcloud/IdEst/Projets/MiscMetabar/R",
  pattern = "*.R",
  full.names = TRUE
)
sapply(file.sources, source, .GlobalEnv)
```


```{r}
load("output/all_phyloseq_data_for_ecology")
```



## Exploration and cleaning of samples metadata

### Run Number

```{r}
d_asv_ecm@sam_data$Run_clean <- d_asv_ecm@sam_data$Run
d_asv_ecm@sam_data$Run_clean[grepl("6_", d_asv_ecm@sam_data$Run_clean)] <- 6
```

### Soil type

```{r}
d_asv_ecm <- subset_samples(d_asv_ecm, !d_asv_ecm@sam_data$Type_sol %in% c("Blanc", NA))
```

```{r, results='asis'}
kable(table(d_asv_ecm@sam_data$Type_sol),  
      caption = "Number of sample in function of soil type")
```

### Soil dilution

```{r}
kable(table(na.exclude(d_asv_ecm@sam_data$Dilution_sol)),  
      caption = "Number of sample in function of soil dilution")
```

### Plants species

```{r}
kable(table(na.exclude(d_asv_ecm@sam_data$Sp)),  
      caption = "Number of sample in function of plant species")
```

### Samples Types

```{r}
kable(table(na.exclude(d_asv_ecm@sam_data$Type_echantillon)),  
      caption = "Number of sample in function of samples types")
```


### Samples Types * plants species

```{r}
kable(table(d_asv_ecm@sam_data$Type_echantillon, d_asv_ecm@sam_data$Sp),  
      caption = "Number of sample in function of samples types * plant species")
```

### Soil Types * plants species

```{r}
kable(table(d_asv_ecm@sam_data$Type_sol, d_asv_ecm@sam_data$Sp),  
      caption = "Number of sample in function of samples types * plant species")
```

### Soil Types * Samples types

```{r}
kable(table(d_asv_ecm@sam_data$Type_sol, d_asv_ecm@sam_data$Type_echantillon),  
      caption = "Number of sample in function of samples types * sample types")
```


### Description of samples metadata in function of Questions

```{r}
mermaid("
graph LR
    A[Tous les échantillons]-->B[Témoins négatifs]
    B --> B1[Blanc d'extraction] 
    B --> B2[Blanc]
    B --> B3[Témoins stérile]
    A -.-> Q1(Méthodo)
    Q1 --> Q1a((Sol))
    Q1 --> Q1b((Apex))
    Q1 --> Q1c((Bain))
    A -.-> Q2(Dilution) 
    Q1a -- éch. partagés --- Q2
    Q1b -- éch. partagés --- Q2
    A -.-> Q3(Résilience)
    Q3 -- éch. partagés --- Q1

  style Q1 fill:#E5E25F;
  style Q2 fill:#E5E25F;
  style Q3 fill:#E5E25F;

")
```


```{r}
mermaid("
graph LR
  subgraph Résilience
    Che[Chênaie]---Sol
    Che---Apex
    Apex-.-> Qi[Quercus ilex] 
    Apex-.-> Cm[Cistus monspeliensis]
    Apex-.-> Au[Arbutus unedo]  
    Apex-.-> Ph[Pinus halepensis]
    Urb[Urbain]---Sol
    Urb---Apex
    Gar[Garrigues]---Sol
    Gar---Apex

  end
")
```

------------------------------------------------------------------------

# Soil alpha diversity in function of soil type and plant species

------------------------------------------------------------------------


```{r}
plot_richness(d_asv_ecm, "Type_sol", color = "Sp", 
              measures = c("Observed", "Chao1", "ACE", "Shannon", "Simpson"))
```


```{r}
p_list <- hill_phyloseq(subset_samples(d_asv_ecm, d_asv_ecm@sam_data$Type_sol %in%
                                         c("Chenaie","Garrigue", "Urbain")), 
                        "Type_sol", color_fac="Type_sol", letters = T)
multiplot(p_list[[1]], p_list[[2]], p_list[[3]], cols = 1)
```



```{r}
p_list <- hill_phyloseq(subset_samples(d_asv_ecm, d_asv_ecm@sam_data$Sp %in%
                                         c("Arbutus_unedo", "Cistus_monspeliensis",
                                           "Pinus_halepensis", "Quercus_ilex")), 
                        "Sp", color_fac="Sp", letters = T)
multiplot(p_list[[1]], p_list[[2]], p_list[[3]], cols = 1)
```



```{r results='asis'}
res <- adonis_phyloseq(
  physeq = d_asv_ecm,
  formula = "Type_echantillon + Type_sol * Sp"
)
gt(round(res$aov.tab, 3), rownames_to_stub = TRUE,
   caption="Résultat de l\'analyse de variance sur les distances (Permanova)")
```





------------------------------------------------------------------------

# Résilience des sols urbains

------------------------------------------------------------------------

:::question
Qu'elle est la communauté de champignons ECM présentent dans un sol après 50 ans
sous le béton?
:::

------------------------------------------------------------------------

# Dilution

------------------------------------------------------------------------


:::question
Quelle proportion de sol de garrigue ou de chênaie suffit pour ensemencer un sol urbain?
Combien doit-on réintroduire de sol pour favoriser le réaménagement?

- Diversité alpha en fonction des dilutions
- Diversité béta en fonction des dilutions
:::

## Filter dataset

TODO  : trouver une bonne manière de sélectionner seulement les échantillons nécéssaires à cette analyse...

```{r}
d_dilution <- 
  subset_samples(d_asv_ecm, !is.na(d_asv_ecm@sam_data[,"Dilution_sol"]) | 
                   d_asv_ecm@sam_data$Type_echantillon %in% c("Sol", "Mycorhize"))
d_dilution <- clean_physeq(d_dilution)
```

## Alpha-diversity

```{r}
# p_list <- hill_phyloseq(d_dilution, "Dilution_sol", color_fac="Dilution_sol", letters = T)
# multiplot(p_list[[1]], p_list[[2]], p_list[[3]], cols = 1)
```


```{r results='asis'}
# res <- adonis_phyloseq(
#  physeq = d_dilution,
#  formula = "Type_echantillon + Sp + as.factor(Dilution_sol)"
#)
#gt(round(res$aov.tab, 3), rownames_to_stub = TRUE,
#   caption="Résultat de l\'analyse de variance sur les distances (Permanova)")
```





------------------------------------------------------------------------

# Analyse Méthodologique (NGS sol - pool d'apex)

------------------------------------------------------------------------

:::question
Quelles sont les différentes visions des communautés ECM en fonction du type d'échantillon? 

:::
