

clean_physeq <-  function(physeq){
  if (!is.null(physeq@refseq)){
    if (sum(!names(physeq@refseq) %in% taxa_names(physeq)) > 0){
      names(physeq@refseq) <- taxa_names(physeq)
      message("Change the names in refseq slot")
    }
  }
  if (!is.null(physeq@tax_table)){
    if (sum(!rownames(physeq@tax_table) %in% taxa_names(physeq)) > 0){
      rownames(physeq@tax_table) <- taxa_names(physeq)
      message("Change the names in tax_table slot")
    }
  }
  
  if (!is.null(physeq@sam_data)){
    if (sum(!rownames(physeq@sam_data) %in% sample_names(physeq)) > 0){
      rownames(physeq@sam_data) <- sample_names(physeq)
      message("Change the names in sam_data slot")
    }
  }
  
  new_physeq <- physeq
  if (sum(taxa_sums(new_physeq) == 0) > 0) {
    # new_otu_table <- otu_table(new_physeq, taxa_are_rows =T)[,taxa_sums(new_physeq) > 0]
    # new_tax_table <- tax_table(new_physeq)[taxa_sums(new_physeq) > 0,]
    # new_physeq <- merge_phyloseq(new_otu_table, new_tax_table, physeq)
    
    new_physeq <- subset_taxa(physeq, taxa_sums(physeq)>0)
  }
  if (sum(sample_sums(new_physeq) == 0) > 0) {
    new_physeq <- subset_samples(new_physeq, sample_sums(physeq) > 0)
  }
  message(paste("Supress", ntaxa(physeq)-ntaxa(new_physeq), "taxa and", 
                nsamples(physeq)-nsamples(new_physeq), 
                "samples.")
  )
  return(new_physeq)
}



path <- ("C:/Users/MALLET/Desktop/STAGE-2022/DATA_ECM/")
taxa_are_rows = FALSE

if (file.exists(paste(path, "/otu_table.csv", sep = ""))) {
    otu_table_csv <- as.matrix(utils::read.csv2(paste(path, "/otu_table.csv", sep = "")))
    rownames(otu_table_csv) <- otu_table_csv[,1]
    otu_table_csv <- otu_table_csv [,-1]
    otu_table_csv <- apply(otu_table_csv, 2, as.numeric)
    physeq <- phyloseq(otu_table(otu_table_csv, taxa_are_rows = taxa_are_rows))
  }
if (file.exists(paste(path, "/refseq.csv", sep = ""))) {
    dna <- Biostrings::DNAStringSet(utils::read.csv2(paste(path, "/refseq.csv", sep = ""))[,2])
    names(dna) <- utils::read.csv2(paste(path, "/refseq.csv", sep = ""), sep=";")[,1]
    physeq <- phyloseq::merge_phyloseq(physeq, refseq(dna))
  }
if (file.exists(paste(path, "/tax_table.csv", sep = ""))) {
    tax_table_csv <- utils::read.csv(paste(path, "/tax_table.csv", sep = ""))
    rownames(tax_table_csv) <- tax_table_csv[,1]
    tax_table_csv <- as.matrix(tax_table_csv [,-1])
    physeq <- phyloseq::merge_phyloseq(physeq, tax_table(tax_table_csv) )
  }
if (file.exists(paste(path, "/sam_data.csv", sep = ""))) {
    sam_data_csv <- utils::read.csv2(paste(path, "/sam_data.csv", sep = ""))
    physeq <- phyloseq::merge_phyloseq(physeq, sample_data(sam_data_csv) )
  }

d_brut <-  physeq

d_brut_ordinate<- ordinate(d_brut, method = "NMDS", dist="bray")
plot_ordination(d_brut, d_brut_ordinate, type="samples", color="Sp")

#type






## psychedelic
funky <- colorRampPalette(c("#A6CEE3","#1F78B4","#B2DF8A",
                            "#33A02C","#FB9A99","#E31A1C",
                            "#FDBF6F","#FF7F00","#CAB2D6",
                            "#6A3D9A","#FFFF99","#B15928"))


###########
## fac2col
###########
## translate a factor into colors of a palette
## colors are randomized based on the provided seed
fac2col <- function(x, col.pal=funky, na.col="transparent", seed=NULL){
  ## get factors and levels
  x <- factor(x)
  lev <- levels(x)
  nlev <- length(lev)
  
  ## get colors corresponding to levels
  if(!is.null(seed)){
    set.seed(seed)
    newseed <- round(runif(1,1,1e9))
    on.exit(set.seed(newseed))
    col <- sample(col.pal(nlev))
  } else {
    col <- col.pal(nlev)
  }
  
  ## get output colors
  res <- rep(na.col, length(x))
  res[!is.na(x)] <- col[as.integer(x[!is.na(x)])]
  
  ## return
  return(res)
}




################################################################################
################################################################################
                      QUESTIONS
################################################################################
################################################################################


d_sol<- subset_samples(d_brut, d_brut@sam_data$Type_sol %in% c("Chenaie", "Urbain", "Garrigue") &
                        d_brut@sam_data$Type_echantillon =="Sol" & d_brut@sam_data$Run %in% 1:2)
sum(colSums(d_sol@otu_table)==0)
d_sol<-clean_physeq(d_sol)

View(as.matrix(d_sol@sam_data))


ordination<- ordinate(d_sol, method = "MDS", dist="bray")
plot_ordination(d_sol, ordination, type="samples", color="Type_sol")

plot_ordination(d_sol, ordination, type="taxa", color = "growthForm")

ordination<- ordinate(as_binary_otu_table(d_sol), method = "MDS", dist="jaccard")
plot_ordination(as_binary_otu_table(d_sol), ordination, type="samples", color="Type_sol")

plot_bar(d_sol, x = "Type_sol", fill="Genus", facet_grid=~Order)
plot_bar(d_sol, x = "Type_sol", fill="Order")
plot_bar(as_binary_otu_table(d_sol), x = "Type_sol", fill="Genus", facet_grid=~Order)

tax_datatable(d_sol)
sankey_phyloseq(d_sol, "Type_sol")
otu_circle(d_sol, "Type_sol", rarefy = F)
otu_circle(d_sol, "Type_sol", taxa = "Family", rarefy = T)
otu_circle(d_sol, "Type_sol", taxa = "growthForm", rarefy = T)

otu_circle(d_sol, "Type_sol", taxa = "growthForm", nb_seq = F, rarefy=T)
otu_circle(d_sol, "Type_sol", taxa = "Family", nb_seq = F, rarefy=T)


p <- plot_bar(as_binary_otu_table(d_sol), x = "Type_sol", facet_grid=~"growthForm", fill = "Family") 


p$data$couleur_remplissage <- fac2col(p$data$Order)


ggplot(p$data, aes(fill=growthForm, x=Type_sol, y = Abundance)) +
  geom_bar( stat="identity", position = "fill") 

p <- plot_bar(d_sol, x = "Type_sol", facet_grid=~"growthForm", fill = "Family") 
ggplot(p$data, aes(fill=growthForm, x=Type_sol, y = Abundance)) +
  geom_bar( stat="identity", position = "fill") 


p <- plot_bar(d_sol, x = "Type_sol", facet_grid=~"growthForm", fill = "Family") 
ggplot(p$data, aes(fill=growthForm, x=Type_sol, y = Abundance, col=Class)) +
  geom_bar( stat="identity") + scale_fill_manual(values = funky(12))

# plutot faire un KK test (car conditions d'applications non validées)
lm1 <- aov(lm(vegan::renyi((d_sol@otu_table), scale=0, hill = T) ~ sqrt(apply(d_sol@otu_table, 1, sum)) + d_sol@sam_data$Type_sol))

# conditions d'application
par(mfrow=c(2,2))
summary(lm1)        
par(mfrow=c(1,2))

p <- hill_phyloseq(d_sol, "Type_sol")

p[[1]]
p[[2]]
p[[3]]
p[[4]]
p$plot_tuckey$data

rs_med_sol=aggregate(p$plot_Hill_0$data$Hill_0,list(p$plot_Hill_0$data$Type_sol),FUN=median)
rs_med_sol
div_med_sol=aggregate(p$plot_Hill_1$data$Hill_1,list(p$plot_Hill_1$data$Type_sol),FUN=median)
div_med_sol

rs_moy_sol=aggregate(p$plot_Hill_0$data$Hill_0,list(p$plot_Hill_0$data$Type_sol),FUN=mean)
rs_moy_sol
div_moy_sol=aggregate(p$plot_Hill_1$data$Hill_1,list(p$plot_Hill_1$data$Type_sol),FUN=mean)
div_moy_sol

rs_sd_sol=aggregate(p$plot_Hill_0$data$Hill_0,list(p$plot_Hill_0$data$Type_sol),FUN=sd)
rs_sd_sol
div_sd_sol=aggregate(p$plot_Hill_1$data$Hill_1,list(p$plot_Hill_1$data$Type_sol),FUN=sd)
div_sd_sol

metriques_sol=data.frame(rs_med_sol,rs_moy_sol[,2],rs_sd_sol[,2],div_med_sol[,2],div_moy_sol[,2],div_sd_sol[,2])
colnames(metriques_sol)=c("Type_sol","rs_med_sol","rs_moy_sol","rs_sd_sol","div_med_sol","div_moy_sol","div_sd_sol")
# rajouter N echantillon / type de sol


res <- adonis_phyloseq(
  physeq = d_sol,
  formula = "Type_sol * Pt_temporel"
)


gt(round(res$aov.tab, 3), rownames_to_stub = TRUE,
   caption="Résultat de l\'analyse de variance sur les distances (Permanova)")


MiscMetabar::physeq_heat_tree(d_sol, node_size = n_obs,
                              node_color = n_obs,
                              node_label = taxon_names,
                              tree_label = taxon_names,
                              node_size_trans = "log10 area",
                              taxonomic_level = c(1:7))


# nbre d,ASV présent dans x échantillons (par ex. 213 ASV sont dans un seul échantillon)
table(sort(colSums(d_sol@otu_table>0)))

# nbre de séquences par ASV (par ex. l'ASV_7 totalise 26689 séquences)
table(sort(colSums(d_sol@otu_table)))



physeq <- d_sol
fact = "Type_sol"
min_nb_seq = 0

moda <- as.factor(unlist(unclass(physeq@sam_data[, fact])[fact]))
if (length(moda) != dim(physeq@otu_table)[1]) {
  data_venn <- t(apply(physeq@otu_table, 1, function(x) {
    by(x, moda, max)
  }))
}
if (length(moda) != dim(physeq@otu_table)[2]) {
  data_venn <- t(apply(t(physeq@otu_table), 1, function(x) {
    by(x, moda, max)
  }))
}


combinations <- data_venn > min_nb_seq
e <- new.env(TRUE, emptyenv())
cn <- colnames(combinations)
for (i in seq.int(dim(combinations)[1])) {
  if (any(combinations[i, ])) {
    ec <- paste(cn[combinations[i, ]], collapse = "&")
    e[[ec]] <- if (is.null(e[[ec]])) {
      1L
    }
    else {
      (e[[ec]] + 1L)
    }
  }
}
en <- ls(e, all.names = TRUE)
weights <- as.numeric(unlist(lapply(en, get, e)))
combinations <- as.character(en)
table_value <- data.frame(combinations = as.character(combinations), 
                          weights = as.double(weights))



table_value
pourc_venn=table_value$weights
pourc_venn=pourc_venn/sum(pourc_venn)*100
pourc_venn # % d'ASV partagees
dataframe_venn=data.frame(table_value$combinations,pourc_venn) 


# 